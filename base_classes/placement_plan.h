#ifndef PLACEMENT_PLAN_H
#define PLACEMENT_PLAN_H

#include <QObject>
#include <string.h>
#include "placement_element.h"
#include "floor_plan.h"
#include <vector>

//Placement plan - сопоставляет товары по слотам, после создания не может быть изменён,содержит вектор placement_element
//копию которого может возвратить при необходимости. Для создания класса требуется vector placement_array , создаваемый статическим методом
//create_placement_array из Floor_plan.В Placement_array задаётся соответствие товаров слотам.

class Placement_plan
{

public:
    //Placement plan автоматически создаётся на основе Floor plan //Единажды созданный план не изменяется
    static Placement_plan* create_placement_plan(std::string name,std::string description,std::vector<Placement_element>* placement_array);
    ~Placement_plan();
    //Добавить массив товар на слот

    //Защищенный доступ к полю класса, для работы с имеющимися зонами(Placement_elements)
    std::vector<Placement_element> get_used_Placement_elements();//копия от рабочий план размещения
    std::string get_name();
    std::string get_description();


    static std::vector<Placement_element> *create_placement_array(Floor_plan* floor_plan);

private:
    Placement_plan();
    std::string name;
    std::string description;
    std::vector<Placement_element>* used_Placement_elements;
    Placement_plan(std::string name,std::string description,std::vector<Placement_element>* placement_array);

};

#endif // placement_Plan_H
