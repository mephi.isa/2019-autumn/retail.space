#include "placement_plan.h"

Placement_plan::Placement_plan(std::string name, std::string description, std::vector<Placement_element> *placement_array)
{
    if(name != ""){
        this->name = name;
    }
    else{
        this->name = "Unnamed";
    }

    if(description != ""){
        this->description = description;
    }
    else{
        this->description = "Unnamed";
    }
    this->used_Placement_elements = placement_array;
}

std::vector<Placement_element> Placement_plan::get_used_Placement_elements()
{
    return *this->used_Placement_elements;
}

std::string Placement_plan::get_name()
{
    return this->name;
}

std::string Placement_plan::get_description()
{
    return this->description;
}

std::vector<Placement_element> *Placement_plan::create_placement_array(Floor_plan *floor_plan)
{
    std::vector<Area> Areas = floor_plan->get_used_Area ();

    if(Areas.size () == 0){
        return nullptr;
    }

    std::vector<Placement_element> *Placement_array = new std::vector<Placement_element>;
    for(unsigned long i = 0; i<Areas.size ();i++){
        std::vector<Slot> Slots = Areas[i].get_used_Slots ();
        for(unsigned int j =0 ; j < Slots.size ();j++){
            Placement_element tmp_element(&Slots[j], static_cast<unsigned int>(i) );
            Placement_array->push_back (tmp_element);
        }
    }
    return Placement_array;
}

Placement_plan *Placement_plan::create_placement_plan(std::string name, std::string description, std::vector<Placement_element> *placement_array)
{
    if(placement_array == nullptr){
        return nullptr;
    }
   Placement_plan *PP = new Placement_plan(name,description, placement_array);
   return PP;
}

Placement_plan::~Placement_plan()
{
    free(this->used_Placement_elements);
}

