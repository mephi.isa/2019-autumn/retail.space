#ifndef SLOT_H
#define SLOT_H

#include <QObject>

//Slot -  элементарный класс составляющий Floor_plan , описывает ячейку, ее положение и категорию

typedef struct {
    unsigned x;
    unsigned y;
    unsigned z;
}slot_position;


class Slot
{
public:
    Slot(int category, slot_position position);
    int get_slot_Category();
    slot_position get_slot_Position();
    Slot();
private:
    slot_position position;
    int category;//Как устанавливать категорию?
};

#endif // SLOT_H
