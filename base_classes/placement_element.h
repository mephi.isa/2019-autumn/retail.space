#ifndef PLACEMENT_ELEMENT_H
#define PLACEMENT_ELEMENT_H

#include <QObject>
#include "slot.h"

//Placement_element - составная часть Placement_plan, сопоставляет слот и товар, содержит информацию о слоте, зоне и товаре.

class Placement_element
{
public:
    int set_product_ID(unsigned int product_ID);
    unsigned int get_product_ID();
    unsigned int get_area_ID();
    slot_position get_position();

private:
    Placement_element();
    Placement_element(Slot *slot, unsigned int area_ID);
    Slot slot;
    unsigned int area_ID;
    unsigned int product_ID = 0;
    friend class Placement_plan;
    friend class Test_Placement_element;
};

#endif // PLACEMENT_ELEMENT_H
