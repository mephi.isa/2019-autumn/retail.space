#ifndef TEST_PLAN_H
#define TEST_PLAN_H

#include <QObject>
#include <QTest>

#include "floor_plan.h"

class Test_Floor_plan : public QObject
{
    Q_OBJECT
public:
    explicit Test_Floor_plan(QObject *parent = nullptr);

private slots: // должны быть приватными

    void test_get_name();
    void test_get_description();
    void test_apply();
    void test_add_Area();
    void test_add_same_Area();
};

#endif // TEST_PRODUCT_H
