#include "test_slot.h"
#include "slot.h"

Test_Slot::Test_Slot(QObject *parent) :
    QObject(parent)
{
}

void Test_Slot::test_get_slot_Category()
{
    Slot s(1,{1,1,1});
    QCOMPARE(s.get_slot_Category (),1);
}

void Test_Slot::test_get_slot_Position()
{
    Slot s(1,{1,1,1});
    slot_position sp = s.get_slot_Position ();
    QCOMPARE(sp.x,1U);
    QCOMPARE(sp.y,1U);
    QCOMPARE(sp.z,1U);

}
