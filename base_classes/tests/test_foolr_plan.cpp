#include "test_floor_plan.h"


Test_Floor_plan::Test_Floor_plan(QObject *parent) :
    QObject(parent)
{

}

void Test_Floor_plan::test_get_name()
{

    std::string name = "test_name",description = "test_description";
    Floor_plan f("test_name","test_description",1);
    QCOMPARE(f.get_name (),"test_name");
}

void Test_Floor_plan::test_get_description()
{
    std::string name = "test_name",description = "test_description";
    Floor_plan f("test_name","test_description",1);

    QCOMPARE(f.get_name (),"test_name");
}

void Test_Floor_plan::test_add_same_Area()
{
    std::string name = "test_name",description = "test_description";
    Floor_plan f("test_name","test_description",1);

    area_coordinates min,max;
    slot_position max_slot_pos;

    max_slot_pos.x = 10; max_slot_pos.y = 10; max_slot_pos.z = 10;
    min.x =1; min.y =1; max.x =10; max.y = 10;

    Area *A = Area::create_area(name,description,min,max,max_slot_pos);

    QCOMPARE(f.add_Area (A),0);
    QCOMPARE(f.add_Area (A),-1);


}

void Test_Floor_plan::test_apply()
{
    std::string name = "test_name",description = "test_description";
    Floor_plan f("test_name","test_description",1);

    area_coordinates min,max;
    slot_position max_slot_pos;

    max_slot_pos.x = 10; max_slot_pos.y = 10; max_slot_pos.z = 10;
    min.x =1; min.y =1; max.x =10; max.y = 10;

    Area *A = Area::create_area(name,description,min,max,max_slot_pos);
    f.add_Area (A);

    f.apply ();

    max_slot_pos.x = 5; max_slot_pos.y = 5; max_slot_pos.z = 5;
    min.x =15; min.y =15; max.x =20; max.y = 20;

    Area *B = Area::create_area(name,description,min,max,max_slot_pos);
    QCOMPARE(f.add_Area (B),-1);
}

void Test_Floor_plan::test_add_Area()
{
    std::string name = "test_name",description = "test_description";
    Floor_plan f("test_name","test_description",1);

    area_coordinates min,max;
    slot_position max_slot_pos;

    max_slot_pos.x = 10; max_slot_pos.y = 10; max_slot_pos.z = 10;
    min.x =1; min.y =1; max.x =10; max.y = 10;

    Area *A = Area::create_area(name,description,min,max,max_slot_pos);

    QCOMPARE(f.add_Area (A),0);
    QCOMPARE(f.get_used_Area ().size (),1);
}
