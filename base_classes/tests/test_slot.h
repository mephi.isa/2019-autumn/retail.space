#ifndef TEST_SLOT_H
#define TEST_SLOT_H

#include <QObject>
#include <QTest>
#include "slot.h"

class Test_Slot : public QObject
{
    Q_OBJECT
public:
    explicit Test_Slot(QObject *parent = nullptr);

private slots: // должны быть приватными
    void test_get_slot_Category();
    void test_get_slot_Position();

};

#endif // TEST_PRODUCT_H

