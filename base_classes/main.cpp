﻿#include <QCoreApplication>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <QTest>

#include "floor_plan.h"
#include "placement_plan.h"

#include "tests/test_slot.h"
#include "tests/test_placement_element.h"
#include "tests/test_area.h"
#include "tests/test_floor_plan.h"
#include "tests/test_placement_plan.h"

using namespace std;

int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);
    QTest::qExec(new Test_Slot,argc,argv);
    QTest::qExec(new Test_Placement_element,argc,argv);
    QTest::qExec(new Test_Area,argc,argv);
    QTest::qExec(new Test_Floor_plan,argc,argv);
    QTest::qExec(new Test_Placement_plan,argc,argv);
    return a.exec();
}


