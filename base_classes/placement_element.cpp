#include "placement_element.h"

Placement_element::Placement_element(Slot *slot,unsigned int area_ID)
{
    this->slot = *slot;
    this->area_ID = area_ID;
}

int Placement_element::set_product_ID(unsigned int product_ID)
{
    this->product_ID = product_ID;
    return 0;
}

unsigned int Placement_element::get_product_ID()
{
    return this->product_ID;
}

unsigned int Placement_element::get_area_ID()
{
    return this->area_ID;
}

slot_position Placement_element::get_position()
{
    return this->slot.get_slot_Position ();
}
