QT -= gui
QT+=testlib

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    area.cpp \
    floor_plan.cpp \
        main.cpp \
    placement_element.cpp \
    placement_plan.cpp \
        slot.cpp \
    tests/test_area.cpp \
    tests/test_foolr_plan.cpp \
    tests/test_placement_element.cpp \
    tests/test_placement_plan.cpp \
        tests/test_slot.cpp \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    area.h \
    floor_plan.h \
    placement_element.h \
    placement_plan.h \
    slot.h \
    tests/test_area.h \
    tests/test_floor_plan.h \
    tests/test_placement_element.h \
    tests/test_placement_plan.h \
    tests/test_slot.h \

SUBDIRS += \




