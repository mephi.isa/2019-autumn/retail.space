#include "slot.h"

Slot::Slot(int category, slot_position position)
{
    this->category = category;
    this->position = position;
}

int Slot::get_slot_Category()
{
    return this->category;
}

slot_position Slot::get_slot_Position()
{
    return this->position;
}

Slot::Slot()
{

}
